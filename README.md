# Projektor

Template [`Moloch`](https://github.com/jolars/moloch).

## Avantages

- Utilisation de Moloch (fork de Metropolis)
- Meilleures couleurs de sombre
- 2 styles d'affichages concernant les sections

## Usage

Placer le fichier [`projektor.sty`](./projektor.sty) au même niveau que votre `.tex`.

Dans le fichier `.tex` :

```latex
\usepackage[option]{projektor}
```

Options disponibles :

|  Option  | Utilité                                            |
| :------: | :------------------------------------------------- |
|  `dark`  | Thème sombre                                       |
|  `code`  | Utilisation de `minted`                            |
|   `fr`   | Utilisation de `babel`                             |
|  `sec`   | Table des matières lors d'une nouvelle partie      |
| `subsec` | Table des matières lors d'une nouvelle sous-partie |

> C'est mieux si `subsec` est toujours activé avec `sec` (l'inverse n'est pas vrai).

Commandes disponibles :

|                 Commande                  | Utilité                                                                                   |
| :---------------------------------------: | :---------------------------------------------------------------------------------------- |
|               `\ul{texte}`                | Souligne du texte                                                                         |
| `\hl<>{texte}` ou `\hl<>[couleur]{texte}` | Met en surbrillance du texte, `<>` permet de choisir sur quelles diapos appliquer l'effet |
|   `\code[minted-opts]{lang}{filepath}`    | Ajoute un fichier via minted `\inputminted` et l'ajoute en pièce jointe du fichier        |

## [Exemples](./examples)

### [Normal](./examples/light.tex)

|             Diapositive              |           Titre de partie            |
| :----------------------------------: | :----------------------------------: |
| ![](https://i.imgur.com/9FdNPUp.png) | ![](https://i.imgur.com/OOl4DlM.png) |

### [Sombre](./examples/dark.tex)

|             Diapositive              |           Titre de partie            |
| :----------------------------------: | :----------------------------------: |
| ![](https://i.imgur.com/bVVOl2R.png) | ![](https://i.imgur.com/bsMvgaB.png) |

### [Plan à chaque partie](./examples/section.tex)

> Ce mode a comme effet de rendre inopérant `\section*{}`.

|             Diapositive              |         Titre de la partie 1         |      Titre de la sous-partie 2       |
| :----------------------------------: | :----------------------------------: | :----------------------------------: |
| ![](https://i.imgur.com/ZwjnduV.png) | ![](https://i.imgur.com/nFibwLR.png) | ![](https://i.imgur.com/xDjpFM3.png) |
